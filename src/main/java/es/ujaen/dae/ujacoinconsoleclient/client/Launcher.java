/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoinconsoleclient.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 *
 * @author adria
 */
public class Launcher {

    public static void main(String[] args) throws IOException, SecurityException {

        UjaCoinClient cliente = new UjaCoinClient();

        cliente.menuLogin();

    }

}
