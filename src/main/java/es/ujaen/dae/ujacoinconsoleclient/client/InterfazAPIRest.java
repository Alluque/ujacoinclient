/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoinconsoleclient.client;

import es.ujaen.dae.ujacoinconsoleclient.entidades.DTO.ClienteDTO;
import es.ujaen.dae.ujacoinconsoleclient.util.ErrorInterfaz;
import es.ujaen.dae.ujacoinconsoleclient.util.Pair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author adria
 */
@Component
public class InterfazAPIRest {

    HttpClient httpClient;
    ClientHttpRequestFactory reqFactory;
    RestTemplate restTemplate;

    //DNI del usuario
    private String dni;

    public InterfazAPIRest(String dni, String pass) {

        this.dni = dni;

        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials
                = new UsernamePasswordCredentials(dni, pass);
        provider.setCredentials(AuthScope.ANY, credentials);

        httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

        reqFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

        restTemplate = new RestTemplate(reqFactory);

    }

    public Pair<ErrorInterfaz, ClienteDTO> checkConexion() {

        String url = "https://localhost:8080/ujacoin/clientes/{dni}";
        ResponseEntity<ClienteDTO> response = null;

        try {

            response = restTemplate.getForEntity(url, ClienteDTO.class, dni);

        } catch (Exception ex) {

            if (ex instanceof org.springframework.web.client.HttpClientErrorException && ex.getMessage().startsWith("404")) {
                return new Pair<>(ErrorInterfaz.ErrorCredenciales, null);
            } else {
                return new Pair<>(ErrorInterfaz.ErrorConexion, null);
            }
        }

        return new Pair<>(ErrorInterfaz.OK, response.getBody());
    }

    public Pair<ErrorInterfaz, ClienteDTO> actualizarCliente(ClienteDTO cliente) {
        String url = "https://localhost:8080/ujacoin/clientes/{dni}";
        ResponseEntity<ClienteDTO> response = null;

        try {
            
            restTemplate.put(url, cliente, dni);
            
            response = restTemplate.getForEntity(url, ClienteDTO.class, dni);

        } catch (Exception ex) {

            if (ex instanceof org.springframework.web.client.HttpClientErrorException && ex.getMessage().startsWith("404")) {
                return new Pair<>(ErrorInterfaz.ErrorCredenciales, null);
            } else {
                return new Pair<>(ErrorInterfaz.ErrorConexion, null);
            }
        }

        return new Pair<>(ErrorInterfaz.OK, response.getBody());
    }
}
