/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoinconsoleclient.client;

import es.ujaen.dae.ujacoinconsoleclient.entidades.DTO.ClienteDTO;
import es.ujaen.dae.ujacoinconsoleclient.util.ErrorInterfaz;
import es.ujaen.dae.ujacoinconsoleclient.util.Pair;
import java.util.Scanner;

/**
 *
 * @author adria
 */
public class UjaCoinClient {

    private InterfazAPIRest interfaz;
    ClienteDTO usuario;

    Scanner scanner = new Scanner(System.in);

    public void limpiarConsola() {

        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");

    }

    public void menuLogin() {

        Pair<ErrorInterfaz, ClienteDTO> respuesta;
        System.out.println("Bienvenido al cliente de consola del banco UjaCoin");

        do {
            System.out.println("Por favor escriba su DNI (incluyendo la letra): ");
            String dni = scanner.nextLine();

            System.out.println("Y su contraseña:");
            String pass = scanner.nextLine();

            System.out.println("\nComprobando credenciales...");
            interfaz = new InterfazAPIRest(dni, pass);

            respuesta = interfaz.checkConexion();
            limpiarConsola();

            if (respuesta.first == ErrorInterfaz.ErrorCredenciales) {
                System.out.println("Credenciales incorrectas");
            }

            if (respuesta.first == ErrorInterfaz.ErrorConexion) {
                System.out.println("Error conexion");
            }

        } while (respuesta.first != ErrorInterfaz.OK);

        usuario = respuesta.second;

        System.out.println("Login correcto");

        menuUsuario();

    }

    private void menuUsuario() {

        int opcion = 0;

        do {
            limpiarConsola();
            System.out.println("Usuario " + usuario.getNombre());
            System.out.println("Acciones:");
            System.out.println("1->Detalles de usuario.");
            System.out.println("2->Listado de cuentas.");
            System.out.println("3->Listado de tarjetas.");
            System.out.println("4->Salir");
            System.out.println("Elija una opción:");

            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    menuDetalleUsuario();
                    break;

                case 2:
                    menuCuentas();
                    break;

                case 3:
                    menuTarjetas();
                    break;

                case 4:
                    System.out.println("Adios.");
                    break;

                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }

        } while (opcion != 4);
    }

    private void menuDetalleUsuario() {

        int opcion = 0;
        limpiarConsola();

        do {
            System.out.println("Nombre:\t\t" + usuario.getNombre());
            System.out.println("DNI:\t\t" + usuario.getDni());
            System.out.println("Dirección:\t" + usuario.getDireccion());
            System.out.println("Teléfono:\t" + usuario.getTelefono());
            System.out.println("Email:\t\t" + usuario.getEmail());
            System.out.println("F.Nac:\t\t" + usuario.getFechaNacimiento());
            System.out.println("Acciones:");
            System.out.println("1->Editar datos personales.");
            System.out.println("2->Salir");
            System.out.println("Elija una opción:");

            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    menuEditarDatos();
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }

        } while (opcion != 2);

    }

    private void menuEditarDatos() {

        System.out.println("Nueva dirección:");
        usuario.setDireccion(scanner.nextLine());
        System.out.println("Nuevo email:");
        usuario.setEmail(scanner.nextLine());
        System.out.println("Nuevo teléfono:");
        usuario.setTelefono(scanner.nextLine());

        //Comprobar datos introducidos antes de enviarlos al servidor
        Pair<ErrorInterfaz, ClienteDTO> respuesta = interfaz.actualizarCliente(usuario);

        if (respuesta.first == ErrorInterfaz.OK) {
            
            usuario = respuesta.second;
            
        }

    }

    private void menuCuentas() {

    }

    private void menuTarjetas() {

    }

}
