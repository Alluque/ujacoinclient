/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoinconsoleclient.util;

/**
 *
 * @author adria
 */
public enum ErrorInterfaz {
    OK,
    ErrorConexion,
    ErrorCredenciales,
    ErrorFormatoDatos
}
