/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoinconsoleclient.entidades.DTO;

/**
 * Cuenta de UjaCoins
 *
 * @author Adrian
 */
public class CuentaDTO {

    private String numero;
    private float saldo;

    public CuentaDTO() {
    }

    public CuentaDTO(String numero, float saldo) {
        this.numero = numero;
        this.saldo = saldo;
    }

    public String getNumero() {
        return numero;
    }

    public float getSaldo() {
        return saldo;
    }

}
